﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class MicrophoneListener : MonoBehaviour
{
	public bool IsWorking = true;
	private bool _lastValueOfIsWorking;

	public bool RaltimeOutput = true;
	private bool _lastValueOfRaltimeOutput;
	private string microName = "";

	private AudioSource _audioSource;
	private float _lastVolume = 0;


	public void MicroListenerStartFunction ()
	{
		_audioSource = GetComponent<AudioSource> ();
	}

	public void MicroPhoneStart()
	{
		//	#if !UNITY_EDITOR
		IsWorking = true;
		RaltimeOutput = true;
		WorkStart();
		//	#endif
	}

	public void MicroPhoneStop ()
	{
		_audioSource.Stop ();
		RaltimeOutput = false;
		IsWorking = false;
	}

	public void Update()
	{
	//	#if !UNITY_EDITOR
		if(EveryplayTest.StartMicroPhone) 
		{
				MicroPhoneStart ();

		} else {

				MicroPhoneStop ();
	
		}
			CheckIfIsWorkingChanged();
			CheckIfRealtimeOutputChanged();

		//#endif
	}

	public void CheckIfIsWorkingChanged()
	{
		if (_lastValueOfIsWorking != IsWorking)
		{
			if (IsWorking)
			{
				WorkStart();
			}
			else
			{
				WorkStop();
			}
		}

		_lastValueOfIsWorking = IsWorking;
	}

	public void CheckIfRealtimeOutputChanged()
	{
		if (_lastValueOfRaltimeOutput != RaltimeOutput)
		{
			DisableSound(RaltimeOutput);
		}

		_lastValueOfRaltimeOutput = RaltimeOutput;
	}

	public void DisableSound(bool SoundOn)
	{
		if (SoundOn)
		{
			if (_lastVolume > 0)
			{
				_audioSource.volume = _lastVolume;
			}
			else
			{
				_audioSource.volume = 1f;
			}
		}
		else
		{
			_lastVolume = _audioSource.volume;
			_audioSource.volume = 0f;
		}
	}

	private void WorkStart()
	{

		#if !UNITY_EDITOR
			foreach (string device in Microphone.devices) {
				Debug.Log("Name: " + device);
				microName = device;
			}
			_audioSource.clip = Microphone.Start(microName, true, 10, 44100);
			_audioSource.loop = true;
		while (!(Microphone.GetPosition(microName) > 0))
			{
				_audioSource.Play();
			}
		#endif
		#if UNITY_EDITOR
		_audioSource.clip = Microphone.Start(null, true, 10, 44100);
		_audioSource.loop = true;
		while (!(Microphone.GetPosition(null) > 0))
		{
			_audioSource.Play();
		}
		#endif
	}

	private void WorkStop()
	{
		#if !UNITY_EDITOR
		Microphone.End(microName);
		#endif
		#if UNITY_EDITOR
		Microphone.End(null);
		#endif
	}



}
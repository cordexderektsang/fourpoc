﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateAndScaleController : MonoBehaviour {
	private SetUpImageTarget setUpImageTarget;
	private DistanceClass distanceClass;
	public MicrophoneListener microphoneListener;
	public float lastPosX;
	private const float ROTATE_SPEED = 0.2f;
	private DynamicLoadMarker dynamicLoadMarker;

	// Use this for initialization
	void Start () 
	{
		GlobalStaticVariable.ResetAllGlobalVariabler ();
		setUpImageTarget = GetComponent<SetUpImageTarget> ();
		distanceClass = GetComponent<DistanceClass> ();
		dynamicLoadMarker = GetComponent<DynamicLoadMarker> ();
		setUpImageTarget.GetAllImageTargetStart ();
		distanceClass.DistanceClassStart ();
		microphoneListener.MicroListenerStartFunction ();
		dynamicLoadMarker.LoadDataSet ("");
	}
	
	// Update is called once per frame
	void Update () 
	{
		RaycastHit ();
		rotateCheck ();
		onTouchEnd ();
		distanceClass.DistanceClassUpdate ();
	}







	private  void RaycastHit ()
	{
		if (Input.GetMouseButtonDown (0)) {
			RaycastHit hit;
			if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit)) 
			{
				GlobalStaticVariable.RayhitObject = hit.collider.gameObject;
				Debug.Log (GlobalStaticVariable.RayhitObject.name);
			}
		}

	}


	private void rotateCheck()
	{
		if (Input.touchCount == 1) {
	
				if (lastPosX != 0) {
				//float x = Input.GetTouch(0).position.x - lastPosX;
				float x = lastPosX -Input.GetTouch(0).position.x ;
					rotateObject (x);
				}
			lastPosX = Input.GetTouch(0).position.x;

		} else
		if (Input.GetMouseButton(0)) {

			if (lastPosX != 0) {
					float x = lastPosX - Input.mousePosition.x ;
				rotateObject (x);
			}
			lastPosX = Input.mousePosition.x;

		}
	}

	private void onTouchEnd()
	{
		#if UNITY_EDITOR
		if (Input.GetMouseButtonUp(0))
		{
			lastPosX = 0;
		}
		#endif
		#if !UNITY_EDITOR
		if (Input.touchCount == 0)
		{
			lastPosX = 0;
		}
		#endif
	}
	public void rotateObject(float distance)
	{
		if (GlobalStaticVariable.RayhitObject != null) {
			GlobalStaticVariable.RayhitObject.transform.Rotate (new Vector3 ( 0,distance * ROTATE_SPEED,  0), Space.Self);
		}
	}


}

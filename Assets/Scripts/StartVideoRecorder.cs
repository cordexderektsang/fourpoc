﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartVideoRecorder : MonoBehaviour {
	public iVidCapPro recorder ;
	private int framerate;
	// Use this for initialization
	public void buttonStartRecord ()
	{
		#if UNITY_IPHONE

		// "vr" is a reference to the iVidCapPro object.
		recorder.BeginRecordingSession(
			"Spiralocity",                             // name of the video
			500, 500,                       // video width & height in pixels
			30,                                        // frames per second when frame rate Locked/Throttled
			iVidCapPro.CaptureAudio.No_Audio,          // whether or not to record audio
			iVidCapPro.CaptureFramerateLock.Unlocked); // capture type: Unlocked, Locked, Throttled

			// Things are happening here that you want to be recorded.

		// Register a delegate to be called when the video is complete.
		recorder.RegisterSessionCompleteDelegate(HandleSessionComplete);
		// Register a delegate in case an error occurs during the recording session.
		recorder.RegisterSessionErrorDelegate(HandleSessionError);
			
		recorder.EndRecordingSession(
				iVidCapPro.VideoDisposition.Save_Video_To_Album,  // where to put the finished video 
			out framerate);                        // # of video frames recorded

		#elif UNITY_ANDROID


		#endif

	}


	#region UNITY_IPHONE
	// This delegate function is called when the recording session has completed successfully
	// and the video file has been written.
	public void HandleSessionComplete() {
		// Do UI stuff when video is complete.
	}
	// This delegate function is called if an error occurs during the recording session.
	public void HandleSessionError(iVidCapPro.SessionStatusCode errorCode) {
		// Do stuff when an error occurred.
	}

	#endregion



	#region UNITY_ANDROID




	#endregion
}

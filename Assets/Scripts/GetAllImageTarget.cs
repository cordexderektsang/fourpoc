﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class GetAllImageTarget : MonoBehaviour 
{
	private List<ImageTargetInfor> allMarker = new List<ImageTargetInfor>();
	public GameObject cloub;
	public Vector3 vertical = new Vector3(0,0,0);
	public Vector3 hortizontal = new Vector3(0,0,0);

	// start function To inital add in all marker
	public void GetAllImageTargetStart () 
	{
		AddALLImageTargetToList ();
	}

	/// <summary>
	/// Returns all marker infor.
	/// </summary>
	/// <returns>The all marker infor.</returns>
	public List<ImageTargetInfor> ReturnAllMarkerInfor ()
	{
		return allMarker;
	}

	/// <summary>
	/// Adds ALL image target to list.
	/// </summary>
	private void AddALLImageTargetToList ()
	{

		// Get the Vuforia StateManager
		StateManager sm = TrackerManager.Instance.GetStateManager ();

		// Query the StateManager to retrieve the list of
		// currently 'active' trackables 
		//(i.e. the ones currently being tracked by Vuforia)
		IEnumerable<TrackableBehaviour> activeTrackables = sm.GetTrackableBehaviours ();
		//IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours ();

		// Iterate through the list of active trackables
		foreach (TrackableBehaviour tb in activeTrackables) 
		{
			Debug.Log (" add in marker to list Name : " + tb.TrackableName);
			ImageTargetInfor imageTarget = new ImageTargetInfor ();
			imageTarget.markerGameObject = tb.gameObject;
			imageTarget.markerGameObject.name = tb.TrackableName;
			imageTarget.markerTrackableName = imageTarget.markerGameObject.name;
			imageTarget.trackableEventScript = tb;
			imageTarget.defaultTrackableEventHandler = imageTarget.markerGameObject.AddComponent(typeof(CustomTrackableEventHandler)) as CustomTrackableEventHandler;
			GameObject targetObjectForMarker = Instantiate (cloub);
			targetObjectForMarker.transform.parent = imageTarget.markerGameObject.transform;
			targetObjectForMarker.transform.localPosition = Vector3.zero;
			targetObjectForMarker.transform.localEulerAngles = Vector3.zero;
			targetObjectForMarker.transform.localScale = new Vector3 (1, 1, 1);
			renderOnOrOff (imageTarget.defaultTrackableEventHandler.isTrack);
			imageTarget.rendeObject = targetObjectForMarker;
			//((ImageTarget) tb.Trackable).StartExtendedTracking();
			allMarker.Add (imageTarget);
		}

	}


	public void VerticalButton ()
	{
		
		foreach (ImageTargetInfor ImageData in allMarker)
		{
			ImageData.rendeObject.transform.localEulerAngles = vertical;

		}

	}


	public void HortizontalButton ()
	{
		foreach (ImageTargetInfor ImageData in allMarker)
		{
			ImageData.rendeObject.transform.localEulerAngles = hortizontal;

		}
	}


	public void StopExtendTracking ()
	{

		foreach (ImageTargetInfor ImageData in allMarker)
		{
			((ImageTarget)ImageData.trackableEventScript.Trackable).StopExtendedTracking();

		}
			
	}

	public void StartExtendTracking ()
	{

		foreach (ImageTargetInfor ImageData in allMarker)
		{
			((ImageTarget)ImageData.trackableEventScript.Trackable).StartExtendedTracking();

		}

	}



	/// <summary>
	/// Renders the on or off.
	/// </summary>
	/// <param name="status">If set to <c>true</c> status.</param>
	private void renderOnOrOff (bool status)
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(status);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(status);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = status;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = status;
		}


	}

}

	[System.Serializable]
	public class ImageTargetInfor 
	{
			public GameObject markerGameObject;
			public GameObject rendeObject;
			public string markerTrackableName;
			public TrackableBehaviour trackableEventScript;
			public CustomTrackableEventHandler defaultTrackableEventHandler;

	}

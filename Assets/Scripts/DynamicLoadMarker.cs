﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using System.IO;

public class DynamicLoadMarker : MonoBehaviour {


	public string path = "";
	public GameObject targetDisplayObject;
	private string markerDataSet = "VUmarkerTest.xml";
	private string fullPath = "";
	// Use this for initialization
	public void loadTargetImage(string markerName)
	{

		//VuforiaBehaviour vb = GameObject.FindObjectOfType<VuforiaBehaviour>();
		//vb.RegisterVuforiaStartedCallback (() => LoadDataSet(markerName));
		VuforiaARController vuforia = VuforiaARController.Instance;
		vuforia.RegisterVuforiaStartedCallback(() => LoadDataSet(markerName));
	}

	/// <summary>
	/// generate all image object
	/// </summary>
	public void LoadDataSet(string fileName)
	{
		fullPath = Path.Combine (Application.persistentDataPath ,markerDataSet);
		Debug.Log (fullPath);
		//Debug.Log("step 5 load data set with following fileName : " + fileName);
		ObjectTracker objectTracker = TrackerManager.Instance.GetTracker<ObjectTracker>();

		IEnumerable<DataSet> loadedDataSets = objectTracker.GetDataSets();
		DataSet dataSet = objectTracker.CreateDataSet();
		bool isFileExist = File.Exists (fullPath);
		Debug.Log ("Do File Path Exist : " + isFileExist);
		if (dataSet.Load (fullPath, VuforiaUnity.StorageType.STORAGE_ABSOLUTE)) {
			Debug.Log ("Loading");
			objectTracker.Stop ();
			if (!objectTracker.ActivateDataSet (dataSet)) {
				// Note: ImageTracker cannot have more than 100 total targets activated
				Debug.Log ("<color=yellow>Failed to Activate DataSet: " + fullPath + "</color>");
			}

			if (!objectTracker.Start ()) {
				Debug.Log ("<color=yellow>Tracker Failed to Start.</color>");
			}

		} else {

			Debug.Log ("fail");
		}
		foreach (DataSet loadedDataSet in loadedDataSets)
		{
			Debug.Log("testing the path: " + loadedDataSet.Path);

		}

		IEnumerable<TrackableBehaviour> tbs = TrackerManager.Instance.GetStateManager().GetTrackableBehaviours();

		foreach (TrackableBehaviour tb in tbs)
		{

				//if (data.trackableName.Equals (tb.TrackableName)) 
				//{
				// change generic name to include trackable name
				tb.gameObject.name = tb.TrackableName;

				// add additional script components for trackable
				tb.gameObject.AddComponent<CustomTrackableEventHandler>();
				GameObject target = Instantiate (targetDisplayObject);
				Debug.Log ("lala");
				target.transform.parent = tb.gameObject.transform;

				//dataPath.downLoadimageTargetJsonData (tb.TrackableName);										


		}
			
	}

}

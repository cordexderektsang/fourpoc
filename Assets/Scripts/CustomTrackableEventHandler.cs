/*==============================================================================
Copyright (c) 2010-2014 Qualcomm Connected Experiences, Inc.
All Rights Reserved.
Confidential and Proprietary - Protected under copyright and other laws.
==============================================================================*/

using UnityEngine;

namespace Vuforia
{
    /// <summary>
    /// A custom handler that implements the ITrackableEventHandler interface.
    /// </summary>
	public class CustomTrackableEventHandler : MonoBehaviour,
                                                ITrackableEventHandler
    {
        #region PRIVATE_MEMBER_VARIABLES
 
        private TrackableBehaviour mTrackableBehaviour;
    
        #endregion // PRIVATE_MEMBER_VARIABLES


		public bool isTrack = false;

		public bool isHortizontal = false;

        #region UNTIY_MONOBEHAVIOUR_METHODS
    
        void Start()
        {
            mTrackableBehaviour = GetComponent<TrackableBehaviour>();
            if (mTrackableBehaviour)
            {
                mTrackableBehaviour.RegisterTrackableEventHandler(this);
            }
        }

        #endregion // UNTIY_MONOBEHAVIOUR_METHODS



        #region PUBLIC_METHODS


		public void Update ()
		{
			//-150
		/*	if (isTrack) {
				Transform child = this.transform.GetChild (0);
				Debug.Log (this.gameObject.transform.eulerAngles.x + " : " + this.gameObject.transform.eulerAngles.y + " : " + this.gameObject.transform.eulerAngles.z);
				//if (this.gameObject.transform.eulerAngles.y > 250 && this.gameObject.transform.eulerAngles.y < 356) {
				if (this.gameObject.transform.eulerAngles.y > 90) {
					isHortizontal = true;
					child.transform.localEulerAngles = new Vector3(-90,180,0);
					child.transform.localPosition = new Vector3(0,0,-0.5f);
				} else {
					isHortizontal = false;
					child.transform.localEulerAngles = new Vector3(0,0,0);
					child.transform.localPosition = new Vector3(0,0,0);
				}
			}*/

		}
        /// <summary>
        /// Implementation of the ITrackableEventHandler function called when the
        /// tracking state changes.
        /// </summary>
        public void OnTrackableStateChanged(
                                        TrackableBehaviour.Status previousStatus,
                                        TrackableBehaviour.Status newStatus)
        {
            if (newStatus == TrackableBehaviour.Status.DETECTED ||
                newStatus == TrackableBehaviour.Status.TRACKED ||
                newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
            {
                OnTrackingFound();
            }
            else
            {
                OnTrackingLost();
            }
        }

        #endregion // PUBLIC_METHODS



        #region PRIVATE_METHODS


        private void OnTrackingFound()
        {
			isTrack = true;
			isHortizontal = false;
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Enable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = true;
            }

            // Enable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = true;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
			GlobalStaticVariable.AllTrackingItem.Add (this);
        }


        private void OnTrackingLost()
        {
			isTrack = false;
			isHortizontal = false;
            Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
            Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

            // Disable rendering:
            foreach (Renderer component in rendererComponents)
            {
                component.enabled = false;
            }

            // Disable colliders:
            foreach (Collider component in colliderComponents)
            {
                component.enabled = false;
            }

            Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
			GlobalStaticVariable.AllTrackingItem.Remove (this);
        }

        #endregion // PRIVATE_METHODS
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using UnityEngine.UI;

public class DistanceClass : MonoBehaviour {

	#region public
	public float defaultDistanceForDefaultScale = 1;// default distance for scale 1
	public GameObject rainBow;// hard core object
	public GameObject caslte;
	public GameObject uiInteraction;
	public  Text intexteractionText;
	public Transform arCam;


	#endregion

	#region private

	//private AlphabetCoreSystem alphabetCoreSystem;// reference script for alphabetCoreSystem
	private GameObject positionOneObject;// store current first tracked object
	private GameObject positionTwoObject;// store current second tracked object
	private Vector3[] vectors = new Vector3[2];// store the first two tracked objects vector position
	private CustomTrackableEventHandler positionOneTrackScript;// store track event script for reference one
	private CustomTrackableEventHandler positionTwoTrackScript;// store track event script for reference two
	private bool isEnable = false;// single call control for render on and off
	private bool isHorizontal = false;
	public bool thisIsVertical = false;
	private bool isCastleInteraction = false;
	private bool isRainBowInteraction = false;
	private bool isOnClick = false;
	private const string interaction = "Interaction";
	private const string clear = "Clear";
	private Vector3 maxPoint;
	private Vector3 minPoint;
	private float counter;
	private float maxTime = 0.3f;

	#endregion

	public void DistanceClassStart ()
	{

	}

	/// <summary>
	/// Update object position and scale base on distance
	/// </summary>
	public  void DistanceClassUpdate ()
	{
		MarkerDistanaceCalculateFunction ();
	}
	#region call Back For Frame Id Hit


	public void Action ()
	{
	/*	if (isOnClick) {
			isOnClick = false;
			isRainBowInteraction = false;
			isCastleInteraction = false;
			SetRender (false, caslte);
			SetRender (false, rainBow);
			return;
		}*/
			
			isOnClick = true;
		if (isCastleInteraction) 
		{
			ActiveCastleMetod2();
		}
			intexteractionText.text = clear; 
	

	}



	public void ActiveCastleMetod2()
	{
		List<Transform> transformObject = new List<Transform> ();
		if (GlobalStaticVariable.AllTrackingItem.Count >= 5) {
			foreach (CustomTrackableEventHandler tracker in GlobalStaticVariable.AllTrackingItem) {
				transformObject.Add (tracker.gameObject.transform);
			}
			caslte.transform.parent = FindCenterUnit(transformObject,2).transform;
			caslte.transform.localPosition = Vector3.zero;
			caslte.transform.localEulerAngles = Vector3.zero;

			caslte.transform.localScale = new Vector3 (1, 1, 1);
			FindCentroidFromMultiObjects (transformObject);
	
		}
		Vector3 postion = FindAverageFromMultiObjects (transformObject);
		caslte.transform.parent = null;
		//1.5f
		caslte.transform.localPosition = new Vector3 (postion.x,minPoint.y-1.5f,caslte.transform.localPosition.z);
		caslte.transform.localScale = new Vector3 (3, 3, 3);

		SetRender (true, caslte);
		SetRender (false, rainBow);
	}





	public void ActiveRainBow ()
	{
		SetUpTheFirstTwoTarget (GlobalStaticVariable.AllTrackingItem);
		if (positionOneObject != null && positionTwoObject != null) {
			UpdateTwoMarkersPosition ();
		}
		SetRender (false, caslte);

	}

	public void Deactive ()
	{
		uiInteraction.SetActive (false);
		positionOneObject = null;
		positionTwoObject = null;
		positionOneTrackScript = null;
		positionTwoTrackScript = null;
		SetRender (false, rainBow);
		SetRender (false, caslte);
		isEnable = false;
		isCastleInteraction = false;
		isRainBowInteraction = false;
		isOnClick = false;
		intexteractionText.text = interaction; 


	}
	/// <summary>
	/// if word hit fail, assign the first two tracked object into reference variable for update use
	/// if 5 castle
	/// if 2 scale with rainbow
	/// </summary>
	/// <param name="_list">List.</param>
	public void MarkerDistanaceCalculateFunction ()
	{
	/*	if (GlobalStaticVariable.AllTrackingItem.Count == 5) {

			uiInteraction.SetActive (true);
			isCastleInteraction = true;
			//isRainBowInteraction = false;
			if (!isOnClick) {
				Action ();
				intexteractionText.text = interaction; 
			}

		} else*/
		if (GlobalStaticVariable.AllTrackingItem.Count == 2) {
			uiInteraction.SetActive (true);
			//isCastleInteraction = false;
			isRainBowInteraction = true;
			if (!isOnClick) {
				intexteractionText.text = interaction; 
			}
			if (isOnClick && !isCastleInteraction) {
				ActiveRainBow ();
			}

		} else if (/*isRainBowInteraction ||*/Untrack()) {
			//Debug.Log ("Deactive");
			Deactive ();
		} 
			
	}
		

	public bool Untrack ()
	{

		if (GlobalStaticVariable.AllTrackingItem.Count > 2) {
			return false;
			counter = 0;
		} else {
			if (counter < maxTime) {
				
				counter += Time.deltaTime;
				return false;
			} else {
				return true;
			}
		}
	}

	#endregion

	#region set up the first two Object for scale

	/// <summary>
	/// Sets up the first two target for scale
	/// </summary>
	/// <param name="_list">List.</param>
	public void SetUpTheFirstTwoTarget (List<CustomTrackableEventHandler> _list)
	{


		if (_list.Contains (positionOneTrackScript) && _list.Contains (positionTwoTrackScript)) 
		{
			return;
		} 
		else if (_list.Contains (positionOneTrackScript) && !_list.Contains (positionTwoTrackScript)) 
		{
			int index = ReturnNoUseTarget (_list);
			positionTwoObject = _list [index].gameObject;
			positionTwoTrackScript = _list [index];
			return;
		}
		else if (!_list.Contains (positionOneTrackScript) && _list.Contains (positionTwoTrackScript)) 
		{
			int index = ReturnNoUseTarget (_list);
			positionOneObject = _list [index].gameObject;
			positionOneTrackScript = _list [index];
			return;

		}
		else {

			positionOneObject = _list [0].gameObject;
			positionOneTrackScript = _list [0];
			positionTwoObject = _list [1].gameObject;
			positionTwoTrackScript = _list [1];

		}
	
	}

	/// <summary>
	/// return an index that different to current two object
	/// </summary>
	/// <returns>The no use target.</returns>
	/// <param name="_list">List.</param>
	public int ReturnNoUseTarget (List<CustomTrackableEventHandler> _list)
	{
		int j = -1;
		for(int i = 0 ; i < _list.Count; i ++)
		{
			if (_list [i] != positionOneTrackScript && _list [i] != positionTwoTrackScript) 
			{
				return i;
			}
		}
		return j ;

	}

	#endregion

	#region core Position Update Fucntion

	/// <summary>
	/// Update Object position and scale base on two markers distance
	/// as well as disable render base on current for those two markers
	/// </summary>
	private void UpdateTwoMarkersPosition ()
	{
		if (positionOneTrackScript.isTrack && positionTwoTrackScript.isTrack || positionOneTrackScript == null && positionTwoTrackScript == null) {
			float distance = GetDistance (positionOneObject, positionTwoObject);
			vectors = new Vector3[2];
			vectors [0] = positionOneObject.transform.position;
			vectors [1] = positionTwoObject.transform.position;
			Vector3 centerPoint = CenterOfVectors (vectors);
			Vector3 offSetcenterPoint = new Vector3 (centerPoint.x,centerPoint.y,centerPoint.z*0.65f);
			float scale = GetScale (distance)/2;
			rainBow.transform.localPosition = offSetcenterPoint;
			rainBow.transform.eulerAngles =  positionOneObject.transform.eulerAngles;
			rainBow.transform.localScale = new Vector3 (scale, scale, scale);
			if (!IsRenderOn(rainBow)) 
			{
				SetRender (true,rainBow);
				isEnable = true;
			}
		} else {
			if (IsRenderOn(rainBow))
			{
	
				SetRender (false, rainBow);
				isEnable = false;
			}

		}

	}
	#endregion

	//set inital rainbow position when it switch between different orientation
	public void SetRainBow (bool isVertical)
	{
		Transform rainbowChild = rainBow.transform.GetChild (0);
		if (isVertical) {
			
			rainbowChild.localEulerAngles = new Vector3 (90, 0, 0);
			rainbowChild.localPosition = new Vector3 (0,-3.41f,1.5f);
		} else {
			rainbowChild.localEulerAngles = new Vector3 (0, 0.5f, 0);
			rainbowChild.localPosition = new Vector3 (0,5,0);
		}
	}

	//set inital castle position when it switch between different orientation
	public void SetCastle (bool isVertical)
	{
		Transform castleChild = caslte.transform.GetChild (0);
		if (isVertical) {
			//90
			//castleChild.localEulerAngles = new Vector3 (90, 0, 0);
			//castleChild.localPosition = new Vector3 (0,-0.25f,0f);
			//castleChild.localPosition = new Vector3 (0,-1.5f,-0.25f);
			castleChild.localPosition = new Vector3 (0,-2,0);
			castleChild.localEulerAngles = new Vector3 (-90, 180, 0);
			thisIsVertical = true;
		} else {
			//0
			//castleChild.localEulerAngles = new Vector3 (0, 0, 0);
			//castleChild.localPosition = new Vector3 (0,-0.08f,0);
			//castleChild.localPosition = new Vector3 (0,-1.5f,-0.25f);
			castleChild.localPosition = new Vector3 (0,-2,0);
			castleChild.localEulerAngles = new Vector3 (-90, 180, 0);
			thisIsVertical = false;
		}
	}

	#region Math Calculateion

	/// <summary>
	/// Gets the distance between two markers
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistance (CustomTrackableEventHandler a , CustomTrackableEventHandler b)
	{

		float distance = Vector3.Distance(a.gameObject.transform.position, b.gameObject.transform.position);
		return distance;

	}




	/// <summary>
	/// Gets the distance between two objects
	/// </summary>
	/// <returns>The distance.</returns>
	/// <param name="a">The alpha component.</param>
	/// <param name="b">The blue component.</param>
	private float GetDistance (GameObject a , GameObject b)
	{

		float distance = Vector3.Distance(a.transform.position, b.transform.position);
		return distance;

	}

	/// <summary>
	/// Gets the scale aspect from distance
	/// </summary>
	/// <returns>The scale.</returns>
	/// <param name="Distance">Distance.</param>
	private float GetScale (float Distance)
	{
		float scaleAspect = Distance / defaultDistanceForDefaultScale;
		return scaleAspect;
	}

	/// <summary>
	/// get center position between two markers
	/// </summary>
	/// <returns>The of vectors.</returns>
	/// <param name="vectors">Vectors.</param>
	private Vector3 CenterOfVectors( Vector3[] vectors )
	{
		Vector3 sum = Vector3.zero;
		if( vectors == null || vectors.Length == 0 )
		{
			return sum;
		}

		foreach( Vector3 vec in vectors )
		{
			sum += vec;
		}
		return sum/vectors.Length;
	}


	/// <summary>
	/// Finds the centroid from multi objects.
	/// </summary>
	/// <returns>The centroid from multi objects.</returns>
	/// <param name="targets">Targets.</param>
	public void FindCentroidFromMultiObjects ( List< Transform > targets )
	{
		if (targets == null || targets.Count == 0) {
			Debug.Log (" List Is Empty ");
			//return Vector3.zero;
			return;
		}
	
		Vector3 centroid;
		 minPoint = targets[ 0 ].position;
		 maxPoint = targets[ 0 ].position;

		for ( int i = 1; i < targets.Count; i ++ ) {
			Vector3 pos = targets[ i ].position;
			if( pos.x < minPoint.x )
				minPoint.x = pos.x;
			if( pos.x > maxPoint.x )
				maxPoint.x = pos.x;
			if( pos.y < minPoint.y )
				minPoint.y = pos.y;
			if( pos.y > maxPoint.y )
				maxPoint.y = pos.y;
			if( pos.z < minPoint.z )
				minPoint.z = pos.z;
			if( pos.z > maxPoint.z )
				maxPoint.z = pos.z;
		}

		//centroid = minPoint + 0.5f * ( maxPoint - minPoint );

		//return centroid;

	} 

	public Transform FindCenterUnit (List< Transform > targets , int centerNumer)
	{
		List< Transform > reference = targets;
		if (targets == null || targets.Count == 0) {
			Debug.Log (" List Is Empty ");
			return null;
		}

		int index = -1;
		int timeForExeution = 0;
		while (timeForExeution <= centerNumer) 
		{
			float xValue = -1000;
			for (int i = 0; i < reference.Count; i++) 
			{
				if(reference[i].position.x > xValue)
				{
					xValue = reference [i].position.x;
					Debug.Log (xValue);
					index = i;
				}
		
			} 
			if (timeForExeution < centerNumer)
			{
				Debug.Log ("remove " + reference [index].name);
				reference.RemoveAt (index);
			} else {

				Debug.Log ("find " + reference [index].name);
				return reference [index];
			}
			timeForExeution++;

		}	
		return null;

	}

	/// <summary>
	/// Finds the average from multi objects.
	/// </summary>
	/// <returns>The average from multi objects.</returns>
	/// <param name="targets">Targets.</param>
	public Vector3 FindAverageFromMultiObjects ( List< Transform > targets )
	{
		Vector3  center = new Vector3(0, 0, 0);
		float minX = minPoint.x;
		float maxX = maxPoint.x;
		float minY = minPoint.y;
		float maxY = maxPoint.y;
		float minZ = minPoint.z;
		float maxZ = maxPoint.z;
		float count = 0;
		float averageX = 0;

		float offSetY = 0;
		float offSetZ = 0.4f;
		/*for ( int i = 0; i < targets.Count; i ++ ) {
			center += targets[i].position;
			count++;
	
		}*/
	
		averageX = averageValue (minX,maxX);


		//float averageX = ((Mathf.Abs(maxPoint.x) + Mathf.Abs(minPoint.x))/2) + minPoint.x;
		Vector3 Offset = new Vector3 (averageX,minY, minZ - offSetZ);
		Debug.Log ("sum center : " + center + " maxPoint "  + maxPoint + " minPoint " + minPoint + "averageX : " +averageX );

	//	return center / count;
		return Offset;

	}
		

	/// <summary>
	/// calculate average
	/// </summary>
	/// <returns>The value.</returns>
	/// <param name="minX">Minimum x.</param>
	/// <param name="maxX">Max x.</param>
	public float averageValue (float minX, float maxX)
	{
		float average = 0;
		if (maxX > 0 && minX > 0 || minX < 0 && maxX < 0) {

			Debug.Log ("here");
			average = (maxX + minX) / 2;

		} else {
			Debug.Log ("here2");
			float sum = (Mathf.Abs (maxX) + Mathf.Abs (minX));
			float division = 0;
			float answer = 0;
			division = sum / 2;
			if (Mathf.Abs (maxX) > Mathf.Abs (minX)) {
				average = maxX - division;
			} else {
				average = minX + division;
			}
		}
		return average;
	}

	#endregion

	#region Render
	/// <summary>
	/// Sets the render for object.
	/// </summary>
	/// <param name="status">If set to <c>true</c> status.</param>
	/// <param name="thisObject">This object.</param>
	private void SetRender (bool status ,GameObject thisObject)
	{
		Renderer[] rendererComponents = thisObject.GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = thisObject.GetComponentsInChildren<Collider>(true);
		Animation[] animationComponents = thisObject.GetComponentsInChildren<Animation>(true);

		foreach (Renderer component in rendererComponents)
		{
			component.enabled = status;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = status;
		}

	}

	/// <summary>
	/// Determines whether this object is render on or not.
	/// </summary>
	/// <returns><c>true</c> if this instance is render on the specified thisObject; otherwise, <c>false</c>.</returns>
	/// <param name="thisObject">This object.</param>
	private bool IsRenderOn (GameObject thisObject)
	{
		Renderer[] rendererComponents = thisObject.GetComponentsInChildren<Renderer>(true);
		foreach (Renderer component in rendererComponents)
		{
			return component.enabled ;
		}
		return false;
	}
	#endregion

}

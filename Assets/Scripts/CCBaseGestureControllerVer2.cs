﻿using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Basic gesture controller for up to two finger input.
/// <para>
/// Offers one or two finger swiping in four directions - up, down, left and right.
/// </para>
/// <para>
/// Extend this class to implement its core behaviour. Override functions to customize functionality.
/// </para>
/// </summary>
public abstract class CCBaseGestureControllerVer2 : MonoBehaviour
{
    #region Enums and Constants
    public enum TouchState
    {
        None, OneFinger, TwoFinger
    };

    public enum SwipeDirection
    {
        None, Up, Down, Left, Right
    }
    #endregion

    #region Public Variables
    public float horizontalMoveAmount = 20f;
    public float verticalMoveAmount = 20f;
    public float pinchMoveAmount = 10f;
    [Range(5f, 80f)]
    public float horizontalSwipeAngle = 10f;
    [Range(5f, 80f)]
    public float verticalSwipeAngle = 10f;
    [Range(0.01f, 10f)]
    public float minRotateAngle = 0.1f;
    [Range(1f, 10f)]
    public float rotateSpeed = 1f;
    #endregion

    #region Protected Variables
    protected TouchState state = TouchState.None;

    protected Touch[] startTouches = new Touch[2];
    protected Touch[] localTouches = new Touch[2];

    protected float twoFingerBeginDistance;
    protected float twoFingerBeginAngle;


    protected List<float> fingerMovements = new List<float>(2);
    protected List<float> horizontalMovementAngles = new List<float>(2);
    protected List<float> verticalMovementAngles = new List<float>(2);
    protected float fingerPinchDistance = 0.0f;
    protected float rotatedAngle = 0.0f;
    protected Vector2 touchPosition;

    protected SwipeDirection[] swipeDirections = new SwipeDirection[2];

    protected bool isAnyFingerPinched = false;
    protected bool isAnyFingerMoved = false;
    protected bool isAnyFingerRotated = false;
    #endregion

    #region Abstract Methods
    protected abstract void oneFingerSwipeRight();
    protected abstract void oneFingerSwipeLeft();
    protected abstract void oneFingerSwipeUp();
    protected abstract void oneFingerSwipeDown();

    protected abstract void oneFingerMove();

    protected abstract void twoFingerSwipeRight();
    protected abstract void twoFingerSwipeLeft();
    protected abstract void twoFingerSwipeUp();
    protected abstract void twoFingerSwipeDown();

    protected abstract void twoFingerPinch();
    protected abstract void twoFingerMove();
    protected abstract void twoFingerRotate();
    #endregion

    /// <summary>
    /// MonoBehaviour Start() function.
    /// </summary>
    protected virtual void Start ()
    {
        fingerMovements.Clear();
        fingerMovements.Add(0.0f);
        fingerMovements.Add(0.0f);
        horizontalMovementAngles.Clear();
        horizontalMovementAngles.Add(0.0f);
        horizontalMovementAngles.Add(0.0f);
        verticalMovementAngles.Clear();
        verticalMovementAngles.Add(0.0f);
        verticalMovementAngles.Add(0.0f);
    }

    /// <summary>
    /// MonoBehaviour Update() function.
    /// </summary>
    protected virtual void Update ()
    {

        checkTouchState();

        if (state == TouchState.None)
        {

            touchStateNone();
        }
        else if (state == TouchState.OneFinger)
        {

            touchStateOneFinger();
        }
        else if (state == TouchState.TwoFinger)
        {

            touchStateTwoFinger();
        }
       
    }

    #region Finger State Checking Functions
    /// <summary>
    /// Check the number of current input touches to determine the touched state.
    /// <para>
    /// Once the state is determined, call subsequent functions to check behaviour for the given state.
    /// </para>
    /// </summary>
    private void checkTouchState()
    {
        if (Input.touchCount == 0 && state != TouchState.None)
        {
            checkTouchStateNone();
        }
        // Strict checking of only one or two touches. If more than two touches, no input is registered to prevent unknown behaviour
        else if (Input.touchCount == 1 && state != TouchState.OneFinger)
        {
            checkTouchStateOneFinger();
        }
        else if (Input.touchCount == 2 && state != TouchState.TwoFinger)
        {
            checkTouchStateTwoFinger();
        }
    }

    /// <summary>
    /// This function is called once when entering no touch state.
    /// </summary>
    protected virtual void checkTouchStateNone()
    {
        state = TouchState.None;

        fingerMovements[0] = 0.0f;
        fingerMovements[1] = 0.0f;
        horizontalMovementAngles[0] = 0.0f;
        horizontalMovementAngles[1] = 0.0f;
        verticalMovementAngles[0] = 0.0f;
        verticalMovementAngles[1] = 0.0f;
    }

    /// <summary>
    /// This function is called once when entering one finger touch state.
    /// calculate the distance for a single x touch and y touch(by derek)
    /// </summary>
    protected virtual void checkTouchStateOneFinger()
    {
        if (Input.touchCount == 1)
        {
            state = TouchState.OneFinger;
            // Get initial values
            startTouches[0] = Input.GetTouch(0);
   
        }
        
    }

    /// <summary>
    /// This function is called once when entering two finger touch state.
    /// </summary>
    protected virtual void checkTouchStateTwoFinger()
    {
        state = TouchState.TwoFinger;

        // Get initial values
        startTouches[0] = Input.GetTouch(0);
        startTouches[1] = Input.GetTouch(1);
        twoFingerBeginDistance = Vector2.Distance(startTouches[0].position, startTouches[1].position);
        twoFingerBeginAngle = GetAngleInDegrees(startTouches[0].position.x - startTouches[1].position.x, startTouches[0].position.y - startTouches[1].position.y);
    }
    #endregion

    #region Finger Interaction Functions
    /// <summary>
    /// Function to handle no touch state behaviour.
    /// </summary>
    protected virtual void touchStateNoFinger()
    {
        isAnyFingerPinched = false;
        isAnyFingerMoved = false;
        isAnyFingerRotated = false;
    }

    /// <summary>
    /// No finger touched state entrance function.
    /// </summary>
    protected virtual void touchStateNone()
    {
        //debugLogGUIs[0] = "<checkTouchStateNone>";
        touchStateNoFinger();
    }

    /// <summary>
    /// One finger touched state entrance function.
    /// </summary>
    protected virtual void touchStateOneFinger()
    {
        //debugLogGUIs[0] = "<checkTouchStateOneFinger>";

        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            //debugLogGUIs[0] = "(Began)";
            touchStateOneFingerBegan();
   
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Stationary)
        {
            //debugLogGUIs[0] = "(Stationary)";
            touchStateOneFingerStationary();

        }
        else if (Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            //debugLogGUIs[0] = "(Moved) Finger swipe action";
            touchStateOneFingerMoved();
       
        }
        else if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
        {
            // debugLogGUIs[0] = "(Ended)";
            touchStateOneFingerEndedCanceled();
            touchPosition = Input.GetTouch(0).position;
        }
    }

    /// <summary>
    /// Function to handle the beginning of one finger touched state behaviour.
    /// </summary>
    protected virtual void touchStateOneFingerBegan()
    {
       
    }

    /// <summary>
    /// Function to handle a stationary finger touched state behaviour.
    /// </summary>
    protected virtual void touchStateOneFingerStationary()
    {

    }

    /// <summary>
    /// Function to handle a moved finger touched state behaviour.
    /// </summary>
    protected virtual void touchStateOneFingerMoved()
    {
        localTouches[0] = Input.GetTouch(0);

        float fingerMovedDistanceX0 = localTouches[0].position.x - startTouches[0].position.x;
        float fingerMovedDistanceY0 = localTouches[0].position.y - startTouches[0].position.y;

        float fingerMovedDistanceX0Abs = Mathf.Abs(fingerMovedDistanceX0);
        float fingerMovedDistanceY0Abs = Mathf.Abs(fingerMovedDistanceY0);

        fingerMovements[0] = GetDistance(fingerMovedDistanceX0Abs, fingerMovedDistanceY0Abs);
        horizontalMovementAngles[0] = GetAngleInDegrees(fingerMovedDistanceX0Abs, fingerMovedDistanceY0Abs);
        verticalMovementAngles[0] = GetAngleInDegrees(fingerMovedDistanceY0Abs, fingerMovedDistanceX0Abs);

        swipeDirections[0] = SwipeDirection.None;

        // Check horizontal
        if (fingerMovements[0] > horizontalMoveAmount && horizontalMovementAngles[0] < horizontalSwipeAngle)
        {
            if (fingerMovedDistanceX0 > 0)
            {
                swipeDirections[0] = SwipeDirection.Right;
            }
            else if (fingerMovedDistanceX0 < 0)
            {
                swipeDirections[0] = SwipeDirection.Left;
            }

            isAnyFingerMoved = true;
        }
        // Check vertical
        else if (fingerMovements[0] > verticalMoveAmount && verticalMovementAngles[0] < verticalSwipeAngle)
        {
            if (fingerMovedDistanceY0 > 0 && fingerMovedDistanceY0 > verticalMoveAmount)
            {
                swipeDirections[0] = SwipeDirection.Up;
            }
            else if (fingerMovedDistanceY0 < 0 && fingerMovedDistanceY0 < -verticalMoveAmount)
            {
                swipeDirections[0] = SwipeDirection.Down;
            }

            isAnyFingerMoved = true;
        }

        if (isAnyFingerMoved)
        {
            oneFingerMove();
        }
    }


    /// <summary>
    /// Function to handle a lifted finger touched state behaviour.
    /// <para>
    /// Generally handle finger touch/move behaviour in this function.
    /// </para>
    /// </summary>
    protected virtual void touchStateOneFingerEndedCanceled()
    {
        if (isAnyFingerMoved)
        {
         
         
          
            switch (swipeDirections[0])
            {
                case SwipeDirection.Right:
                    oneFingerSwipeRight();
                    break;
                case SwipeDirection.Left:
       
                    oneFingerSwipeLeft();
                    break;
                case SwipeDirection.Up:
                    oneFingerSwipeUp();
                    break;
                case SwipeDirection.Down:
                    oneFingerSwipeDown();
                    break;
                default:
                    break;
            }
        }
    }

    /// <summary>
    /// Two finger touched state entrance function.
    /// </summary>
    protected virtual void touchStateTwoFinger()
    {
        //debugLogGUIs[0] = "<checkTouchStateTwoFinger>";
        if (Input.touchCount >= 2)
        {

            if (Input.GetTouch(0).phase == TouchPhase.Began && Input.GetTouch(1).phase == TouchPhase.Began)
            {
                //debugLogGUIs[0] = "(Began)";

                touchStateTwoFingerBegan();
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Stationary && Input.GetTouch(1).phase == TouchPhase.Stationary)
            {
                //debugLogGUIs[0] = "(Stationary)";

                touchStateTwoFingerStationary();
            }
            else if (Input.GetTouch(0).phase == TouchPhase.Moved && Input.GetTouch(1).phase == TouchPhase.Moved)
            {
                //debugLogGUIs[0] = "(Moved) Two Finger swipe action";

                touchStateTwoFingerMoved();

            }
            else if ((Input.GetTouch(0).phase == TouchPhase.Ended && Input.GetTouch(1).phase == TouchPhase.Ended)
                || (Input.GetTouch(0).phase == TouchPhase.Canceled && Input.GetTouch(1).phase == TouchPhase.Canceled))
            {
                //debugLogGUIs[0] = "(Ended)";)
          
                touchStateTwoFingerEndedCanceled();
            }
        }
    }

    /// <summary>
    /// Function to handle the beginning of two finger touched state behaviour.
    /// </summary>
    protected virtual void touchStateTwoFingerBegan()
    {

    }

    /// <summary>
    /// Function to handle a stationary two finger touched state behaviour.
    /// </summary>
    protected virtual void touchStateTwoFingerStationary()
    {
      
    }

    /// <summary>
    /// Function to handle a moved two finger touched state behaviour.
    /// </summary>
    protected virtual void touchStateTwoFingerMoved()
    {
        localTouches[0] = Input.GetTouch(0);
        localTouches[1] = Input.GetTouch(1);

        float fingerMovedDistanceX0 = localTouches[0].position.x - startTouches[0].position.x;
        float fingerMovedDistanceY0 = localTouches[0].position.y - startTouches[0].position.y;   

        float fingerMovedDistanceX0Abs = Mathf.Abs(fingerMovedDistanceX0);
        float fingerMovedDistanceY0Abs = Mathf.Abs(fingerMovedDistanceY0);

        fingerMovements[0] = GetDistance(fingerMovedDistanceX0Abs, fingerMovedDistanceY0Abs);
        horizontalMovementAngles[0] = GetAngleInDegrees(fingerMovedDistanceX0Abs, fingerMovedDistanceY0Abs);
        verticalMovementAngles[0] = GetAngleInDegrees(fingerMovedDistanceY0Abs, fingerMovedDistanceX0Abs);

        swipeDirections[0] = SwipeDirection.None;

        // Check horizontal
        if (fingerMovements[0] > horizontalMoveAmount && horizontalMovementAngles[0] < horizontalSwipeAngle)
        {
            if (fingerMovedDistanceX0 > 0)
            {
                swipeDirections[0] = SwipeDirection.Right;
            }
            else if (fingerMovedDistanceX0 < 0)
            {
                swipeDirections[0] = SwipeDirection.Left;
            }

            isAnyFingerMoved = true;
        }
        // Check vertical
        else if (fingerMovements[0] > verticalMoveAmount && verticalMovementAngles[0] < verticalSwipeAngle)
        {
            if (fingerMovedDistanceY0 > 0)
            {
                swipeDirections[0] = SwipeDirection.Up;
            }
            else if (fingerMovedDistanceY0 < 0)
            {
                swipeDirections[0] = SwipeDirection.Down;
            }

            isAnyFingerMoved = true;
        }

        float fingerMovedDistanceX1 = localTouches[1].position.x - startTouches[1].position.x;
        float fingerMovedDistanceY1 = localTouches[1].position.y - startTouches[1].position.y;    

        float fingerMovedDistanceX1Abs = Mathf.Abs(fingerMovedDistanceX1);
        float fingerMovedDistanceY1Abs = Mathf.Abs(fingerMovedDistanceY1);

        fingerMovements[1] = GetDistance(fingerMovedDistanceX1Abs, fingerMovedDistanceY1Abs);
        horizontalMovementAngles[1] = GetAngleInDegrees(fingerMovedDistanceX1Abs, fingerMovedDistanceY1Abs);
        verticalMovementAngles[1] = GetAngleInDegrees(fingerMovedDistanceY1Abs, fingerMovedDistanceX1Abs);

        swipeDirections[1] = SwipeDirection.None;

        // Check horizontal
        if (fingerMovements[1] > horizontalMoveAmount && horizontalMovementAngles[1] < horizontalSwipeAngle)
        {
            if (fingerMovedDistanceX1 > 0)
            {
                swipeDirections[1] = SwipeDirection.Right;
            }
            else if (fingerMovedDistanceX1 < 0)
            {
                swipeDirections[1] = SwipeDirection.Left;
            }

            isAnyFingerMoved = true;
        }
        // Check vertical
        else if (fingerMovements[1] > verticalMoveAmount && verticalMovementAngles[1] < verticalSwipeAngle)
        {
            if (fingerMovedDistanceY1 > 0)
            {
                swipeDirections[1] = SwipeDirection.Up;
            }
            else if (fingerMovedDistanceY1 < 0)
            {
                swipeDirections[1] = SwipeDirection.Down;
            }

            isAnyFingerMoved = true;
        }

        if (isAnyFingerMoved)
        {
            twoFingerMove();
        }

        // rotate Z
        float twoFingerCurrentAngle = GetAngleInDegrees(localTouches[0].position.x - localTouches[1].position.x, localTouches[0].position.y - localTouches[1].position.y);
        rotatedAngle = twoFingerBeginAngle - twoFingerCurrentAngle;

        //CCScreenLogger.LogStatic(twoFingerCurrentAngle.ToString(), 2);
        //CCScreenLogger.LogStatic(rotatedAngle.ToString(), 3);

        if (rotatedAngle > minRotateAngle || rotatedAngle < -minRotateAngle)
        {
            isAnyFingerRotated = true;
        }

        if (isAnyFingerRotated)
        {
            twoFingerRotate();
        }

        float twoFingerCurrentDistance = Vector2.Distance(localTouches[0].position, localTouches[1].position);
        fingerPinchDistance = (twoFingerBeginDistance - twoFingerCurrentDistance) * -1; // inverse as pinching in is positive
        if (Mathf.Abs(fingerPinchDistance) >= pinchMoveAmount)
        {
            twoFingerPinch();
        }

        twoFingerBeginDistance = twoFingerCurrentDistance;

        twoFingerBeginAngle = twoFingerCurrentAngle;
    }

    /// <summary>
    /// Function to handle a lifted two finger touched state behaviour.
    /// <para>
    /// Generally handle finger touch/move behaviour in this function.
    /// </para>
    /// </summary>
    protected virtual void touchStateTwoFingerEndedCanceled()
    {
        // Drag motion
        if (isAnyFingerMoved && swipeDirections[0] == swipeDirections[1] && swipeDirections[0] != SwipeDirection.None)
        {
            if (swipeDirections[0] == SwipeDirection.Right)
            {
                twoFingerSwipeRight();
            }
            else if (swipeDirections[0] == SwipeDirection.Left)
            {
                twoFingerSwipeLeft();
            }
            else if (swipeDirections[0] == SwipeDirection.Up)
            {
                twoFingerSwipeUp();
            }
            else if (swipeDirections[0] == SwipeDirection.Down)
            {
                twoFingerSwipeDown();
            }
        }
        // Rotation
        //else if (isAnyFingerRotated && swipeDirections[0] != SwipeDirection.None && swipeDirections[1] != SwipeDirection.None)
        //{
        //    twoFingerRotate();
        //}
    }
    #endregion

    #region Static Helper Variables
    /// <summary>
    /// Calculate the distance between <paramref name="x"/> and <paramref name="y"/>.
    /// </summary>
    /// <returns>
    /// A float representing the distance in pixels.
    /// </returns>
    /// <param name="x">
    /// The x displacement
    /// </param>
    /// <param name="y">
    /// The y displacement
    /// </param>
    protected static float GetDistance(float x, float y)
    {
        return Mathf.Sqrt((x * x) + (y * y));
    }

    /// <summary>
    /// Calculate the angle relative to position (0, 0) given coordinates <paramref name="x"/> and <paramref name="y"/>.
    /// </summary>
    /// <returns>
    /// A float representing the angle in degrees.
    /// </returns>
    /// <param name="x">
    /// The x coordinate
    /// </param>
    /// <param name="y">
    /// The y coordinate
    /// </param>
    protected static float GetAngleInDegrees(float x, float y)
    {
        return GetAngleInRadians(x, y) / Mathf.PI * 180;
    }

    /// <summary>
    /// Calculate the angle relative to position (0, 0) given coordinates <paramref name="x"/> and <paramref name="y"/>.
    /// </summary>
    /// <returns>
    /// A float representing the angle in radians.
    /// </returns>
    /// <param name="x">
    /// The x coordinate
    /// </param>
    /// <param name="y">
    /// The y coordinate
    /// </param>
    protected static float GetAngleInRadians(float x, float y)
    {
        return Mathf.Atan2(y, x);
    }
    #endregion
}
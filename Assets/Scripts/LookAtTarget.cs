﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtTarget : MonoBehaviour {
	public GameObject targetOne;
	public GameObject targetTwo;
	// Update is called once per frame
	void Update () 
	{
		if (targetOne != null && targetTwo != null) {
			targetOne.transform.LookAt (targetTwo.transform.position);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorController : MonoBehaviour {

	public Animator animation;
	private const string nameForIdle = "";
	private const string nameForActive = "";
	private const string nameForOnClick = "";
	// Use this for initialization


	public void PlayIdleAnimation ()
	{
		bool isIdle = animation.GetBool (nameForIdle);
		if(isIdle){
			return;
		}
		if (!string.IsNullOrEmpty (nameForActive)) {
			animation.SetBool (nameForActive, false);
		}
		if (!string.IsNullOrEmpty (nameForOnClick)) {
			animation.SetBool (nameForOnClick, false);
		}
		if (!string.IsNullOrEmpty (nameForIdle)) {
			animation.SetBool (nameForIdle, true);
			animation.SetTrigger (nameForIdle);
		}
	}

	public void PlayActiveAnimation ()
	{
		bool isActive = animation.GetBool (nameForActive);
		if(isActive){
			return;
		}
		if (!string.IsNullOrEmpty (nameForIdle)) {
			animation.SetBool (nameForIdle, false);
		}
		if (!string.IsNullOrEmpty (nameForOnClick)) {
			animation.SetBool (nameForOnClick, false);
		}
		if (!string.IsNullOrEmpty (nameForActive)) {
			animation.SetBool (nameForActive, true);
			animation.SetTrigger (nameForActive);
		}
	}


	public void PlayOnClickAnimation ()
	{
		bool isOnClick = animation.GetBool (nameForOnClick);
		if(isOnClick){
			return;
		}
		if (!string.IsNullOrEmpty (nameForActive)) {
			animation.SetBool (nameForActive, false);
		}
		if (!string.IsNullOrEmpty (nameForIdle)) {
			animation.SetBool (nameForIdle, false);
		}
		if (!string.IsNullOrEmpty (nameForOnClick)) {
			animation.SetBool (nameForOnClick, true);
			animation.SetTrigger (nameForOnClick);
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;
using UnityEngine.SceneManagement;
public class SwitchCam : MonoBehaviour {

	void RestartCamera() 
	{

		VuforiaARController.Instance.RegisterVuforiaStartedCallback (callBack);
	}




	public void CamSwitch ()
	{
		RestartCamera();
	}

	private void callBack()
	{
		CameraDevice.CameraDirection currentDir = CameraDevice.Instance.GetCameraDirection();
		//VuMarkManager vuMarkManager = TrackerManager.Instance.GetStateManager().GetVuMarkManager();
		//Debug.Log (currentDir.ToString());
		CameraDevice.Instance.Stop();
		CameraDevice.Instance.Deinit();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
	
		if (currentDir == CameraDevice.CameraDirection.CAMERA_BACK || currentDir == CameraDevice.CameraDirection.CAMERA_DEFAULT) {
			CameraDevice.Instance.Init (CameraDevice.CameraDirection.CAMERA_FRONT);
		} else {
			CameraDevice.Instance.Init (CameraDevice.CameraDirection.CAMERA_BACK);
		}
		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = false;
		VuforiaBehaviour.Instance.enabled = false;

		GameObject.FindObjectOfType<VuforiaBehaviour>().enabled = true;
		VuforiaBehaviour.Instance.enabled = true;
		CameraDevice.Instance.Start();
		TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
	}




}

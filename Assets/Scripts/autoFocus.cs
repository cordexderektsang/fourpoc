﻿using UnityEngine;
using System.Collections;

public class autoFocus : MonoBehaviour {

	private float maxTIme = 40; 
	public float countTime = 0;
	private bool isAuto = true;




	// Use this for initialization
	// Update is called once per frame
	void Update () 
	{

		if (countTime < maxTIme && !isAuto) {

			countTime += Time.deltaTime;

		} else {

			isAuto = true;
		}
		if (isAuto) 
		{
			Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
			isAuto = false;
			countTime = 0;
		}
		if(Input.touchCount>= 1)
		{
			Vuforia.CameraDevice.Instance.SetFocusMode (Vuforia.CameraDevice.FocusMode.FOCUS_MODE_TRIGGERAUTO);
			isAuto = false;
			countTime = 0;

		}
	}
}

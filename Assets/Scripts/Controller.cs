﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller : MonoBehaviour {

	public GetAllImageTarget getAllImageTarget;
	public DistanceClass distanceClass;

	// Use this for initialization
	void Start () 
	{
		getAllImageTarget.GetAllImageTargetStart ();
		distanceClass.DistanceClassStart ();
		GlobalStaticVariable.ResetAllGlobalVariable ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		distanceClass.DistanceClassUpdate ();
	}
}

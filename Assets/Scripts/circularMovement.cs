﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class circularMovement : MonoBehaviour {

	public GameObject markerOne;
	public GameObject markerTwo;
	public float radius = 1;
	public  LineRenderer line;
	public List<Vector3> allDrawPoints = new List<Vector3> ();
	//1.02f
	public float centerOffSetX = 1;
	public float centerOffSetZ = 1;
	private float positionX = 0;
	private float positionY = 0;
	private float positionZ = 0;
	private float x;
	private float y;
	private float z = 0;

	private float setPoint = 90;
	private float pointNum = -90;
	private float radiusAspect = 0.5f;
	private float distanceAspect = 1;
	private float distanceReference = -1;
	public bool isDone = false;
	private GameObject mama ;
	public int index = 0;
	// Use this for initialization
	void Start () {
		positionX = markerOne.transform.position.x;
		positionY = markerOne.transform.position.y;
		positionZ = markerOne.transform.position.z;
		mama = GameObject.Find ("mum");
		line.useWorldSpace = false;
	}
	//20 ..1.4
	// Update is called once per frame
	void Update () {
		radius = GetDistance ();
		mama.transform.LookAt (markerTwo.transform);
		//mama.transform.Rotate(new Vector3(0, 0, -90));
		if (!isDone) {
			CircularArroundX ();
		}
		if (distanceReference != radius)
		{
			isDone = false;
		}


//		Debug.Log (Mathf.Sin((210 * Mathf.PI)/180)  + "XXX" + Mathf.Cos((-30 * Mathf.PI)/180));;
	}

	/// <summary>
	/// Circulars object arround x.
	/// </summary>
	private void CircularArroundX ()
	{

		float angle = pointNum ;

		if (pointNum < setPoint) {
			line.SetVertexCount (0);
			index += 1;
			y = Mathf.Cos((angle * Mathf.PI)/180) * (radius / 2);/// sin value and add in current object position with offset 
			z = (((angle + 90) / 180) * (radius));// + (positionZ * aspectZ ); /// cos value and add in current object position with offset 
			//			transform.localPosition = mama.transform.position - new Vector3 (mama.transform.position.x , y, z);
			allDrawPoints.Add(new Vector3 (0 , y, z));
			transform.localPosition = new Vector3 (0 , y, z);

		} else {
			distanceReference = radius;
			pointNum = -90;
			index = 0;
			DrawLine ();
			allDrawPoints.Clear ();
			isDone = true;
		}




		pointNum += 2f;

	}

	private void DrawLine ()
	{

		line.SetVertexCount (allDrawPoints.Count);
		for(int i = 0; i < allDrawPoints.Count; i ++ ){
			line.SetPosition (i,allDrawPoints[i]);

		}
	}



	/// <summary>
	/// force circle circular point start from point one 
	/// centerOffSet is the addition point add into position
	/// radius is the circular area for this object
	/// add in 1 
	/// </summary>
	/// <returns>The circle of set.</returns>
/*	private float GetCircleOfSetX ()
	{
		float aspect = 1 + (centerOffSetX * radius);
		return aspect;
	}*/

	/// <summary>
	/// force circle circular point start from point one 
	/// centerOffSet is the addition point add into position
	/// radius is the circular area for this object
	/// add in 1 
	/// </summary>
	/// <returns>The circle of set.</returns>
	/*private float GetCircleOfSetZ ()
	{
		float aspect = 1 - (centerOffSetZ * radius);
		return aspect;
	}*/


	/// <summary>
	///  convert distance between two mark in to radius
	/// the radiusAspect is get from scene test obervation 1 distance : 0.5 radius
	/// </summary>
	/// <returns>The distance.</returns>
	private float GetDistance ()
	{
		float dist = Vector3.Distance(markerOne.transform.position, markerTwo.transform.position);
		return dist;

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class callNative : MonoBehaviour {
	private static System.Action<AndroidJavaObject> Callback;



	#if !UNITY_EDITOR && UNITY_ANDROID
	private static readonly string GetImageFromGalleryClassName = "com.example.unityplugin";
	private static readonly string GetImageFromGalleryInterfaceName = GetImageFromGalleryClassName + "$UnityCallback";
	private static AndroidJavaClass javaClass;

	public static AndroidJavaClass GetImageFromGallery {
		get {
			if (javaClass == null) {
				javaClass = new AndroidJavaClass (GetImageFromGalleryClassName);
			}
			return javaClass;
		}
		set {
			javaClass = value;
		}
	}

	public class UnityCallback : AndroidJavaProxy
	{
		public UnityCallback () : base (GetImageFromGalleryInterfaceName)
		{
		}

		public void ImageSelectionComplete (AndroidJavaObject jo)
		{
			Debug.Log ("return called");
			if (Callback != null) {
				Callback (jo);
			}
		}
	}
	#endif

	// Use this for initialization
	public void StartRecord ()
	{
		Debug.Log ("start record");
		AndroidJavaClass androidJC = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = androidJC.GetStatic<AndroidJavaObject>("currentActivity");
		AndroidJavaClass plugin = new AndroidJavaClass ("com.example.unityplugin.PluginClass");
		//plugin.CallStatic ("CallStaticForRecord");
		//plugin.Call("CallTest");
		#if !UNITY_EDITOR && UNITY_ANDROID
			plugin.CallStatic("CallStaticForRecord",jo);
		#endif

	}
	

	public void StopVideo ()
	{
		Debug.Log ("stop record");
		var plugin = new AndroidJavaClass ("com.example.unityplugin.PluginClass");
		plugin.CallStatic ("CallStaticForStopRecord");


	}


	private static void CallbackFunction(AndroidJavaObject jo)
	{
		Debug.Log ("hello");
	}

}





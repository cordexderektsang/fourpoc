﻿using UnityEngine;
using System.Collections;

public class CCAndroidHelper
{
	#if !UNITY_EDITOR && UNITY_ANDROID
	private static AndroidJavaClass jc;
	public static AndroidJavaClass UnityAndroidJavaClass
	{
		get
		{
			if (jc == null)
			{
				jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			}
			return jc;
		}
		set
		{
			jc = value;
		}
	}
	
	private static AndroidJavaObject jo;
	public static AndroidJavaObject UnityAndroidCurrentActivity
	{
		get
		{
			if (jo == null)
			{
				jo = UnityAndroidJavaClass.GetStatic<AndroidJavaObject>("currentActivity");
			}
			return jo;
		}
		set
		{
			jo = value;
		}
	}
	#endif
}

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Vuforia;
using System.IO;

public class EveryplayTest : MonoBehaviour
{
    public bool showUploadStatus = true;
	public static bool StartMicroPhone = false;
    private bool isRecording = false;
    private bool isPaused = false;
    private bool isRecordingFinished = false;
    private GUIText uploadStatusLabel;
	public List<GameObject> uIItem = new List <GameObject> ();
	private bool isUIOnOff =true;

    void Awake()
    {
        if (enabled && showUploadStatus)
        {
            CreateUploadStatusLabel();
        }
		Everyplay.Initialize ();
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {
        if (uploadStatusLabel != null)
        {
            Everyplay.UploadDidStart += UploadDidStart;
            Everyplay.UploadDidProgress += UploadDidProgress;
            Everyplay.UploadDidComplete += UploadDidComplete;
        }

        Everyplay.RecordingStarted += RecordingStarted;
        Everyplay.RecordingStopped += RecordingStopped;
    }

    void Destroy()
    {
        if (uploadStatusLabel != null)
        {
            Everyplay.UploadDidStart -= UploadDidStart;
            Everyplay.UploadDidProgress -= UploadDidProgress;
            Everyplay.UploadDidComplete -= UploadDidComplete;
        }

        Everyplay.RecordingStarted -= RecordingStarted;
        Everyplay.RecordingStopped -= RecordingStopped;
    }

    private void RecordingStarted()
    {
        isRecording = true;
        isPaused = false;
        isRecordingFinished = false;
    }

    private void RecordingStopped()
    {
        isRecording = false;
        isRecordingFinished = true;
    }

    private void CreateUploadStatusLabel()
    {
        GameObject uploadStatusLabelObj = new GameObject("UploadStatus", typeof(GUIText));

        if (uploadStatusLabelObj)
        {
            uploadStatusLabelObj.transform.parent = transform;
            uploadStatusLabel = uploadStatusLabelObj.GetComponent<GUIText>();

            if (uploadStatusLabel != null)
            {
                uploadStatusLabel.anchor = TextAnchor.LowerLeft;
                uploadStatusLabel.alignment = TextAlignment.Left;
               // uploadStatusLabel.text = "Not uploading";
            }
        }
    }

    private void UploadDidStart(int videoId)
    {
      //  uploadStatusLabel.text = "Upload " + videoId + " started.";
    }

    private void UploadDidProgress(int videoId, float progress)
    {
       // uploadStatusLabel.text = "Upload " + videoId + " is " + Mathf.RoundToInt((float) progress * 100) + "% completed.";
    }

    private void UploadDidComplete(int videoId)
    {
       // uploadStatusLabel.text = "Upload " + videoId + " completed.";

        StartCoroutine(ResetUploadStatusAfterDelay(2.0f));
    }

    private IEnumerator ResetUploadStatusAfterDelay(float time)
    {
        yield return new WaitForSeconds(time);
      //  uploadStatusLabel.text = "Not uploading";
    }

	public void turnOnOffUI ()
	{
		foreach (GameObject ui in uIItem)
		{
			if (ui.activeSelf) 
			{
				isUIOnOff = false;
				ui.SetActive (false);
			} else {
				isUIOnOff = true;
				ui.SetActive (true);
			}

		}
	}

    void OnGUI()
    {
		if (!isUIOnOff) {
			return;
		}
       /* if (GUI.Button(new Rect(10, 10, 138, 48), "Everyplay"))
        {
            Everyplay.Show();
            #if UNITY_EDITOR
            Debug.Log("Everyplay view is not available in the Unity editor. Please compile and run on a device.");
            #endif
        }

       if (isRecording && GUI.Button(new Rect(10, 64, 138, 48), "Stop Recording"))
        {
            Everyplay.StopRecording();
            #if UNITY_EDITOR
            Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
            #endif
        }
        else if (!isRecording && GUI.Button(new Rect(10, 64, 138, 48), "Start Recording"))
        {

            Everyplay.StartRecording();
            #if UNITY_EDITOR
            Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
            #endif
        }

        if (isRecording)
        {
            if (!isPaused && GUI.Button(new Rect(10 + 150, 64, 138, 48), "Pause Recording"))
            {
                Everyplay.PauseRecording();
                isPaused = true;
                #if UNITY_EDITOR
                Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
                #endif
            }
            else if (isPaused && GUI.Button(new Rect(10 + 150, 64, 138, 48), "Resume Recording"))
            {
                Everyplay.ResumeRecording();
                isPaused = false;
                #if UNITY_EDITOR
                Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
                #endif
            }
        }

        if (isRecordingFinished && GUI.Button(new Rect(10, 118, 138, 48), "Play Last Recording"))
        {
            Everyplay.PlayLastRecording();
            #if UNITY_EDITOR
            Debug.Log("The video playback is not available in the Unity editor. Please compile and run on a device.");
            #endif
        }

        if (isRecording && GUI.Button(new Rect(10, 118, 138, 48), "Take Thumbnail"))
        {
            Everyplay.TakeThumbnail();
            #if UNITY_EDITOR
            Debug.Log("Everyplay take thumbnail is not available in the Unity editor. Please compile and run on a device.");
            #endif
        }

        if (isRecordingFinished && GUI.Button(new Rect(10, 172, 138, 48), "Show sharing modal"))
        {
            Everyplay.ShowSharingModal();
            #if UNITY_EDITOR
            Debug.Log("The sharing modal is not available in the Unity editor. Please compile and run on a device.");
            #endif
        }*/
    }


	public void StartRecordFunction ()
	{
		if (!isRecording) {
			StartMicroPhone = true;
			Everyplay.StartRecording ();
			//uIItem[1].SetActive(true);
			//uIItem[0].SetActive(false);
			Debug.Log("Start");
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}
               

	public void StopRecordFunction ()
	{
		Debug.Log ("isRecording " + isRecording);
		if (isRecording) {
			StartMicroPhone = false;
			Everyplay.StopRecording ();
			//uIItem[2].SetActive(true);
			//uIItem[1].SetActive(false);
			Debug.Log("stop");
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	public void PauseRecordFunction ()
	{
		if (isRecording && !isPaused) {
			Everyplay.PauseRecording ();
			isPaused = true;
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	public void ResumeRecordFunction ()
	{
		if (isRecording && isPaused) {
			Everyplay.ResumeRecording ();
			isPaused = false;
		}
		#if UNITY_EDITOR
		Debug.Log("The video recording is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}


	public void ReplayFunction ()
	{
		if (isRecordingFinished) {
			Everyplay.PlayLastRecording ();
			//uIItem[3].SetActive(true);
			//uIItem[2].SetActive(false);
			Debug.Log("Replay");
		}
		#if UNITY_EDITOR
		Debug.Log("The video playback is not available in the Unity editor. Please compile and run on a device.");
		#endif

	}

	public void ShareModel ()
	{

		Everyplay.ShowSharingModal ();
		Debug.Log(GetVideoPath ());
		//uIItem[0].SetActive(true);
		//uIItem[1].SetActive(false);
		Debug.Log("Share");
	}




	static string GetVideoPath ()
	{
		#if UNITY_IOS

		var root = new DirectoryInfo(Application.persistentDataPath).Parent.FullName;
		var everyplayDir = root + "/tmp/Everyplay/session";

		#elif UNITY_ANDROID
		var root = new DirectoryInfo(Application.temporaryCachePath).FullName;
		//var root = new DirectoryInfo(Application.persistentDataPath).FullName;
		var everyplayDir = root + "/sessions";
		Debug.Log(everyplayDir);
		#endif

		var files = new DirectoryInfo(everyplayDir).GetFiles("*.mp4", SearchOption.AllDirectories);
		var videoLocation = "";

		// Should only be one video, if there is one at all
		foreach (var file in files) {
			#if UNITY_ANDROID
			//videoLocation = "file://" + file.FullName;
			videoLocation = file.FullName;
			#else
			videoLocation = file.FullName;
			#endif
			break;
		}

		return videoLocation;
	}



	public void OnClickShareVideo ()
	{
		Debug.Log ("on Click");
		string path = GetVideoPath();
		Debug.Log (path);
		Debug.Log (" Do file exist : " +File.Exists (path));
		if(!string.IsNullOrEmpty(path))
		{
			NativeShare.Share( path, false, "com.imageTesting.marker3" );
		}

	}


	public void takePicture ()
	{
		StartCoroutine (TakeSSAndShare());
	}


	private IEnumerator TakeSSAndShare()
	{
		yield return new WaitForEndOfFrame();

		Texture2D ss = new Texture2D( Screen.width, Screen.height, TextureFormat.RGB24, false );
		ss.ReadPixels( new Rect( 0, 0, Screen.width, Screen.height ), 0, 0 );
		ss.Apply();

		string filePath = Path.Combine( Application.temporaryCachePath, "shared img.png" );
		File.WriteAllBytes( filePath, ss.EncodeToPNG() );

		NativeShare.Share( filePath, true, "com.imageTesting.marker3" );
	}
}
